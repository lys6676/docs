# 使用准备<a name="ZH-CN_TOPIC_0303986164"></a>

## 前提条件与使用事项<a name="zh-cn_topic_0283137591_section887921944913"></a>

-   数据库状态正常。
-   工具运行过程中，如果系统时间被篡改，可能会造成慢SQL数据采集失效。
-   工具不支持在备节点上采集。
-   使用登录到数据库宿主机上的Linux用户，需要将$**GAUSSHOME/bin**添加到PATH环境变量中，即能够直接运行gsql、gs\_guc、gs\_ctl等数据库运维工具。
-   Python版本建议为Python3.6及以上，且运行环境中已经安装相应依赖，并能够正常启动调优程序。
-   本工具由Agent和Detector组成，Agent和Detector之间通过'http'或者'https'方式传递数据，因此需要保证Agent服务器和Detector服务器之间能够正常通信。
-   Detector模块运行collector和monitor服务， 需要分别启动。
-   如果使用'https'方式进行通信，需要准备CA证书以及Agent和Detector的证书和密钥，并分别放入项目根目录certificate下的ca、agent、collector中，同时将密钥加密密码放入certificate的pwf中，并将其权限设置为600，防止其他用户进行读写操作。用户也可以使用share中的脚本生成证书和密钥。
-   建议用户配置自己的python环境，以免影响其他功能使用（例如使用miniconda）。
-   慢SQL的根因分析功能需要WDR报告，用户需要开启track\_stmt\_stat\_level='OFF,L1', log\_min\_duration\_statement=3000（慢SQL阈值，用户可按需设定），单位ms。
-   如果将detecor和数据库部署在同一服务器上，则collector服务端口不能和数据库本地端口相同，否则将无法启动进程。

## 原理简介<a name="zh-cn_topic_0283137591_section1767203555113"></a>

图1 anomaly\_detection结构图

![](figures/图片16.png)

anomaly\_detection是一个独立于数据库内核之外的工具，其结构如图1所示，anomaly\_detection主要由agent和detector模块组成:

-   agent：数据代理模块，由Source、Channel以及Sink组成。部署在数据库环境上的，用于采集数据库中的指标，并通过http或者https方式将其传送给远端检测器模块。
-   detector：检测器模块，负责收集Agent推送的数据并存储，同时该模块基于时序预测和异常检测等算法对数据库指标进行监控和异常检测，同时提供慢SQL根因服务功能。

## anomaly\_detection的运行和安装方法<a name="section7752113811419"></a>

1.  切换到anomaly\_detection目录下。对于openGauss社区代码来说，该路径在openGauss-server/src/gausskernel/dbmind/tools/anomaly\_detection. 对于已经安装的数据库系统，则该源代码路径为  **$GAUSSHOME**/bin/dbmind/anomaly\_detection.
2.  在当前目录下可以看到 requirements.txt 等文件，通过pip包管理工具根据该 requirements.txt 文件安装依赖：

    ```
    pip install -r requirements.txt
    ```

3.  安装成功后可执行main.py. 以获取帮助信息为例，则可以执行：

    ```
    python main.py --help  # 可以直接通过该命令执行获取帮助的动作，其他功能使用方法类似
    ```


## anomaly\_detection的配置文件说明<a name="section5892154973918"></a>

anomaly\_detection 在运行前需要加载**a-detection.conf**和**metric\_task.conf**两个配置文件，, 可以通过** python main.py  --help **命令查看配置文件路径：

**a-detection.conf**：该配置文件包含agent、server、database、security、forecast、log六个section，参数解释如下：

```
[database]
storage_duration = 12H  # 数据存储时间长度，默认12小时
database_dir = ./data  # 数据存储目录

[security]
tls = False
ca = ./certificate/ca/ca.crt
server_cert = ./certificate/server/server.crt
server_key = ./certificate/server/server.key
agent_cert = ./certificate/agent/agent.crt
agent_key = ./certificate/agent/agent.key

[server]
host = 0.0.0.0  # 服务端IP地址
listen_host = 0.0.0.0
listen_port = 8080
white_host = 0.0.0.0  # IP白名单
white_port = 8000  # 端口号白名单

[agent]
source_timer_interval = 10S  # agent端数据采集频率
sink_timer_interval = 10S  # agent端数据发送频率
channel_capacity = 1000  # 缓冲队列最大长度
db_host = 0.0.0.0  # agent节点IP地址
db_port = 8080  # agent节点端口号
db_type = single  # agent节点类型, single: 单机，cn： CN节点、dn： DN节点

[forecast]
forecast_alg = auto_arima  # 时序预测算法，auto_arima、fbprophet(需要用户自行安装)
[log]
log_dir = ./log  # 日志文件位置
```

metric\_task.conf: 该配置文件包括detector method, os\_expoeterhe trend\_parameter三个sections，参数结束如下：

```
[detector_method]
trend = os_exporter # 用于时序预测的表名
slow_sql = wdr # 用户慢SQL诊断的表名

[os_exporter]
cpu_usage_minimum = 1 # cpu_usage的下限值
cpu_usage_maximum = 10 # cpu_usage的上限值
memory_usage_minimum = 1 # memory_usage的下限值
memory_usage_maximum = 10 # memory_usage的上限值
io_read_minimum = 1
io_read_maximum = 10
io_write_minimum = 1
io_write_maximum = 10
io_wait_minimum = 1
io_wait_maximum = 10
disk_space_minimum = 1
disk_space_maximum = 10

[common_parameter]
data_period = 1000S # 是用于时序预测的历史数据长度，支持整数加时间单位（如：100S、2M、10D）。
interval = 20S # 监控间隔
freq = 3S # 趋势预测频率
period = 2 # 趋势预测长度
```

>![](public_sys-resources/icon-note.gif) **说明：** 
>-   支持的时间单位：
>    -   'S'：second，秒。
>    -   'M'：minute，分。
>    -   'H'：hour，小时。
>    -   'D'：day，天。
>    -   'W'：week，周。
>-   minimum和maximum至少提供一个，不能都不提供。
>-   freq和period共同决定时序预测结果，例子：freq=2S，period=5, 则会预测未来2S、4S、6S、8S、10S的值。
>-   建议参数配置的时候，保证训练数据长度大于预测长度，否则预测效果会受影响。

