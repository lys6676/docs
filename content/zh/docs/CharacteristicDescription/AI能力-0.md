# AI能力<a name="ZH-CN_TOPIC_0000001091443838"></a>

-   **[Predictor: AI查询时间预测](Predictor-AI查询时间预测.md)**  

-   **[X-Tuner: 参数调优与诊断](X-Tuner-参数调优与诊断.md)**  

-   **[SQLdiag: 慢SQL发现](SQLdiag-慢SQL发现.md)**  

-   **[Anomaly-detection：数据库指标采集、预测与异常监控](Anomaly-detection-数据库指标采集-预测与异常监控.md)**  

-   **[Index-advisor：索引推荐](Index-advisor-索引推荐.md)**  

-   **[DeepSQL：库内AI算法](DeepSQL-库内AI算法.md)**  


