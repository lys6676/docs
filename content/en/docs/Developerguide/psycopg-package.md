# Psycopg Package<a name="EN-US_TOPIC_0000001127192085"></a>

The psycopg package is obtained from the release package. Its name is  ****GaussDB-Kernel-V**_xxx_**R**_xxx_**C**_xx_-**_OS version number_**-64bit-Python.tar.gz**.

After the decompression, the following folders are generated:

-   **psycopg2**:  **psycopg2**  library file
-   **lib**:  **lib**  library file

