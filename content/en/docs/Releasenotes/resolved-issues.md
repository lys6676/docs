# Resolved Issues<a name="EN-US_TOPIC_0289899193"></a>

This is the fourth release of openGauss. It does not involve any common vulnerabilities and exposures \(CVEs\).

