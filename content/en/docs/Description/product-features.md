# Product Features<a name="EN-US_TOPIC_0294809831"></a>

openGauss features high performance, availability, security, and maintainability.

-   High performance

    Achieves second-level response to queries of tens of billions of data through key technologies, such as the column-store and vectorized executor.

-   High availability \(HA\)

    Intra-city cross-AZ DR ensures zero data loss and minute-level recovery.

-   High security

    Supports security features such as access control, encryption authentication, database audit, and dynamic data masking to provide comprehensive end-to-end data security protection.

-   Good maintainability

    Supports multiple maintenance methods, such as workload analysis report \(WDR\), slow SQL diagnosis, and session diagnosis, to accurately and quickly locate faults.  It also provides the AI4DB capability and uses AI algorithms to implement database self-tuning, self-monitoring, and self-diagnosis.


